![AWS Lambda Badge](https://img.shields.io/badge/AWS%20Lambda-F90?logo=awslambda&logoColor=fff&style=flat-square)![python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)


**Project Goal:**
As a Senior Engineer responsible for securing our AWS infrastructure, I want to implement an automated system that detects publicly accessible S3 buckets and objects, remediates them by making them private, and sends notifications to the security department about the remediated buckets.

**Services and Components:**

**AWS config:** to monitor Amazon S3 bucket ACLs and policies for compliance violations.

**IAM (Identity and Access Management) Role and policy:** to grants a Lambda function permissions to read S3 bucket policies and send alerts through SNS.

**CloudWatch Event rule:** to triggers the Lambda function when AWS Config detects an S3 bucket ACL or policy violation.

**AWS Lambda:** upon detection of publicly accessible S3 buckets, triggers a remediation process.

**SNS:** to send notification 

**Project Steps:**

1. Under Services, choose AWS config and create a rule. Select the existing AWS managed rules and select the S3_BUCKET_LEVEL_PUBLIC_ACCESS_PROHIBITED then save.

2. Under services, choose SNS and create a topic then subscribe to it with the email protocol in our case. Save the ARN of the topic created for later use in the lambda function.

3. Under services, choose IAM and define the role to grant lambda function the access to SNS, CloudWatch, S3.

4. Under Services, choose CloudWatch and select Rules then create rule, leave the default options and Under event source list select AWS services then Config and finally Config rules compliance change. leave the rest of options as is and save. 

5. Under Services, choose lambda and create a function, select python for the code and use the code in the lambda-function.py. Replace tbe ARN and the ExpectedBucketOwnerId by the right values.

   - Under Configuartion, select permissions and edit to link the role created previously to the function.

   ![lambda iam role](https://gitlab.com/niatoned/implement-automated-s3-security-compliance/-/raw/main/Screenshot_2023-10-11_162514.png)
   - Under trigger, create a trigger and select the previous CloudWatch rule.

   ![trigger](https://gitlab.com/niatoned/implement-automated-s3-security-compliance/-/raw/main/Screenshot_2023-10-11_184731.png)

Test and verify the notification in the emails.

