import json
import boto3

def lambda_handler(event, context):
    client=boto3.client('config')
    response = client.get_compliance_details_by_config_rule(
        ConfigRuleName='s3-bucket-level-public-access-prohibited',
        ComplianceTypes=['NON_COMPLIANT'],
    )
    print(f'{response}')
    raw_results=json.loads(json.dumps(response, default=str))
    eval_result=raw_results.get('EvaluationResults')
    
    #Pull ResourseID from evaluation results
    for item in eval_result:
        non_comp_buckets= item.get('EvaluationResultIdentifier').get('EvaluationResultQualifier').get('ResourceId')

    #Make buckets private
    s3_client = boto3.client('s3')
    response = s3_client.put_public_access_block(
        Bucket=non_comp_buckets,
        PublicAccessBlockConfiguration={
        'BlockPublicAcls': True,
        'IgnorePublicAcls': True,
        'BlockPublicPolicy': True,
        'RestrictPublicBuckets': True
        },
        ExpectedBucketOwner='144933599741'
        )
    print({non_comp_buckets})
    
   #Confirmation message
    sns_client = boto3.client('sns')
    response = sns_client.publish(
        TopicArn='arn:aws:sns:us-east-1:144933599741:S3compliancetopic',
        Message= 'All buckets are private',
        Subject='All buckets are private',
        ),
        
    return str(len(eval_result)) + 'Bucket(s) was found, The issue was remediated. A summary was sent to: niatoned@gmail.com'


Set-AWSCredential `
                 -AccessKey AKIASDPVXHH66FCTNQQ5 `
                 -SecretKey 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 `
                 -StoreAs secrets

public class App{
      public static App app;
      private App(){
      }
      public static App getInstance(){
         if(null == app){
              app = new App();
        }
        return app;
      }
      
}
